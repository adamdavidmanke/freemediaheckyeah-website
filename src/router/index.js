import { createRouter, createWebHistory } from "vue-router"
import HomePage from "@/components/home"
import apa from "@/components/adblock-privacy-antivirus"
import ai from "@/components/android-ios"
import b from "@/components/backups"
import bcm from "@/components/books-comics-manga"
import d from "@/components/downloading"
import e from "@/components/educational"
import ge from "@/components/gaming-emulation"
import m from "@/components/misc"
import mtas from "@/components/movie-tv-anime-sport"
import mpr from "@/components/music-podcast-radio"
import ne from "@/components/non-english"
import t from "@/components/tools"
import tor from "@/components/torrenting"


const routes = [
  {
    path: "/",
    meta: { title: "home" },
    component: HomePage,
  },
  {
    path: "/apa",
    meta: { title: "adblock-piracy-antivirus" },
    component: apa,
  },
  {
    path: "/ai",
    meta: { title: "android-ios" },
    component: ai,
  },
  {
    path: "/b",
    meta: { title: "backup" },
    component: b,
  },
  {
    path: "/bcm",
    meta: { title: "books-comics-manga" },
    component: bcm,
  },
  {
    path: "/d",
    meta: { title: "downloading" },
    component: d,
  },
  {
    path: "/e",
    meta: { title: "educational" },
    component: e,
  },  
  {
    path: "/ge",
    meta: { title: "gaming-emulation" },
    component: ge,
  },
  {
    path: "/m",
    meta: { title: "misc" },
    component: m,
  },
  {
    path: "/m",
    meta: { title: "misc" },
    component: m,
  },
  {
    path: "/mtas",
    meta: { title: "movies-tv-anime-sports" },
    component: mtas,
  },
  {
    path: "/mpr",
    meta: { title: "music-podcast-radio" },
    component: mpr,
  },
  {
    path: "/ne",
    meta: { title: "non-english" },
    component: ne,
  },
  {
    path: "/tor",
    meta: { title: "torrenting" },
    component: tor,
  },
  {
    path: "/t",
    meta: { title: "tools" },
    component: t,
  }
]


const router = createRouter({ history: createWebHistory(), routes })

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})

export default router